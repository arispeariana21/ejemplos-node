const http = require('http');

/*
Es un callback que tiene los 2 parametros:
request = Es lo que se esta solicitando, toda la informacion de la url que se solicita,
los headers de la peticion, argumentos
response = Es lo que el servidor le va a responder al cliente
*/

//puerto: camino para acceder al servidor

http.createServer((request, response) => {

  console.log(request);

  response.setHeader('Content-Disposition', 'attachment; filename=lista.csv');
  response.writeHead(200, {'Content-Type': 'application/csv'});
  
  //Escribir respuesta
  response.write('id, nombre\n');
  response.write('2, Mercedes\n');
  response.write('3, Juan\n');
  response.write('4, Pedro\n');


  //Terminar respuesta
  response.end();
})

.listen(8080);

console.log('Escuchando el puerto', 8080);