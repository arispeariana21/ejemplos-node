const express = require('express');
const app = express();
const port = 8080;

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.send('Home Page')
});
  
app.get('/hola-mundo', (req, res) => {
  res.send('Hola mundo en su respectiva ruta')
});

app.get('/generic', (req, res) => {
  res.sendFile(__dirname + '/public/generic.html');
});

app.get('/elements', (req, res) => {
  res.sendFile(__dirname + '/public/elements.html');
});
  
app.get('*', (req, res) => {
  res.send('404 | Page not found')
});
  
app.listen(port, () => {
  console.log(`app escuchando en http://localhost:${port}`);
});
  