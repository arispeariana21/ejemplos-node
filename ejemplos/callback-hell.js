const empleados = [
  {
    id: 1,
    nombre: 'Eleonor'
  },

  {
    id: 2,
    nombre: 'Fausto'
  },

  {
    id: 3,
    nombre: 'Juliana'
  }
];


const salarios = [
  {
    id: 1,
    salario: 1000
  },

  {
    id: 2,
    salario: 1500
  }

];

const getSalario = (id, callback) => {
 const salario = salarios.find(e => e.id === id);
  if(salario){
    callback (null, salario);
  } else {
   callback `No existe salario para id ${id}`;
  }
};

getEmpleado(id, (err, empleado) => {
  if(err){
    console.log('ERROR!');
    return console.log(err);
  }
  console.log('Empleado existe');
  console.log(empleado);


  getSalario(id, (err, salario) => {
    if(err){
      console.log('ERROR!');
      return console.log(err);
    }
    console.log('El empleado:', empleado, 'tiene un salario de:', salario);
  });

});
  
    

