const { Router } = require('express');
const { usuarioGet, usuarioPut } = require('../controllers/user');
const router = Router();

router.get('/', usuarioGet);

router.put('/:id', usuarioPut);

router.post('/', (req, res) => {
  res.status(201).json({
    msg: 'post API'
  });
});

router.delete('/', (req, res) => {
  res.json({
    msg: 'delete API'
  });
});

module.exports = router;

