//const { mostrarMenu, pausa } = require('./helpers/mensajes');
const { 
  inquirerMenu, 
  pausa, 
  leerInput,
  listadoTareasBorrar,
  confirmar,
  mostrarListadoChecklist
} = require('./helpers/inquirer');
const Tarea = require('./helpers/models/tarea');
const Tareas = require('./helpers/models/tareas');
require('colors');

const { guardarDB, leerDB } = require('./helpers/guardarArchivo')



  const main = async () => {
  console.log('hola mundo');

  let opt = '';
  const tareas = new Tareas();

  const tareasDB = leerDB() // el return data, viene aqui (array de Json)
console.log(tareasDB); //imprimiria esto, pero hay un clear en el Inquirer,JS

  if(tareasDB){
      //establecer las tareas
      tareas.cargarTareasFormArray(tareasDB);
  }

  // await pausa()


  do{
    opt = await inquirerMenu(); //de aqui imprime el titulo en verde, y se retorna la opcion seleccionada, en la variable "opt"

    console.log(opt); //saldra el numero de opcion seleccionada
    
    //const tarea = new Tarea('Comprar comida') //PRUEBAS
    // console.log(tareas); //PRUEBAS
    // if (opt !== '7') await pausa(); //PRUEBAS

    switch(opt) { //Switch de la opcion seleccionada.
      case '1': //entra aqui, cuando seleccionamos la primer opcion.
        //crear opcion
        const desc = await leerInput('Descripcion:'); //en desc se guarda lo del LEERINPUT
        console.log(desc); //imprime lo que escribimos
        tareas.crearTarea(desc)
      break;
      case '2':
        console.log(tareas.listadoArr);
         tareas.listadoCompleto(); //este pone una lista de numeros, sumandole al index +1
        //  console.log(tareas._listado);
      break;
      case '3':
        tareas.listarPendientesCompletadas(true)
      break
      case '4':
        tareas.listarPendientesCompletadas(false)
      break;
      case '5':
        const ids = await mostrarListadoChecklist(tareas.listadoArr);
        // console.log(ids);
        tareas.toggleCompletadas(ids);
      break;
      case '6':
        const id = await listadoTareasBorrar(tareas.listadoArr)

        if (id !== '0'){
          const ok = await confirmar('Estas seguro?');
        console.log({ok}); //con esto se ve si sale true or false
        // console.log('alba');
        if(ok){
          tareas.borrarTareas(id);
          console.log('Tarea borrada');
        }
        }
        break;

      default:
        break
    }

    guardarDB(tareas.listadoArr) //luego se guarda
    await pausa()
    
  } while(opt !== '7');

};

main();
