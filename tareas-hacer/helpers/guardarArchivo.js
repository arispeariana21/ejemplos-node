
const fs = require('fs');


//la carpeta db debe existir
const archivo = './db/data.json';

const guardarDB = (data) => {

  //JSON.stringify convierte un objeto JSON en una cadena de texto JSON 
  fs.writeFileSync(archivo, JSON.stringify(data)) //si en data le pasamos un json, este lo convierte en una cadena de texto json

};
const leerDB = () => {
  //existsSync verifica si la ruta de la carpeta existe
  if(!fs.existsSync(archivo)){
      return null;
     }

     //leer archivo, necesitamos la ruta (archivo), 'utf-8' significa que podemos escribir cualquier tipo de letras que hay (mayus, min, caracteres especiales, numeros, todo)
     const info = fs.readFileSync(archivo, {encoding: 'utf-8'})
     //INFO es un array de jsons pero STRING

     //convierte una cadena de JSON a un JSON
     const data = JSON.parse(info);
     //DATA es un array de Json pero ahora como Json
     return data

};

module.exports = {
  guardarDB,
  leerDB
};