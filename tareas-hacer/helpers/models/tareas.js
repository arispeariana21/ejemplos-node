const Tarea = require('./tarea')

class Tareas {

  constructor () {
    this._listado = {};
  }
 
  crearTarea(desc = ''){
    const tarea = new Tarea(desc);
    this._listado[tarea.id] = tarea //como es un json, se le tiene que dar clave y valor
  }

  get listadoArr() {  //este nos va devolver el uuid de las tareas
    const listado = [];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key => {
      // console.log(key);
      const tarea = this._listado[key];
      listado.push(tarea);
    });
    // console.log('alba');
    return listado;
  }

  cargarTareasFormArray(tareas = []){
    //aqui se van a mandar varias tareas, y con el foreach se obtiene la tarea
    tareas.forEach(tarea => {
      this._listado[tarea.id] = tarea;
    });
  }

  listadoCompleto() {
    this.listadoArr.forEach((tarea, index) => {
      const idx = `${index +1}`.green; 
      const { desc, completadoEn } =  tarea //aqui se esta desestructurando el objeto Tarea
      const estado = completadoEn ? 'Completada'.green : 'Pendiente'.red;

      console.log(`${idx} ${desc} :: ${estado}`);
    });
  }

  listarPendientesCompletadas(completadas = true) {
    console.log();
    let contador = 0;

    this.listadoArr.forEach(tarea => {

      const { desc, completadoEn } = tarea;
      const estado = completadoEn ? 'Completada'.green : 'Pendiente'.red;

      if(completadas) {
        if (completadoEn){
          contador += 1;
          console.log(`${(contador + '.').green} ${desc} :: ${completadoEn.green}`)
        }
      } else {
        if(!completadoEn) { //aqui entraria si fuera false
          contador += 1;
          console.log(`${(contador + '.').green} ${desc} :: ${estado}`);
        }
      }
    });
  }

  borrarTareas(id = ''){ //Aqui se le pasa un id
    if (this._listado[id]){ //aqui verifica si _listado tiene el id que le he pasado
      delete this._listado[id] //y asi se borra el listado de un json
    }
  }

  toggleCompletadas(ids = []) {
    ids.forEach(id => {
      const tarea = this._listado[id];
      if(!tarea.completadoEn){
        tarea.completadoEn = new Date().toISOString();

      }
    });
    this.listadoArr.forEach(tarea => {
      if(!ids.includes(tarea.id)){
        this._listado[tarea.id].completadoEn = null;
      }
    });
  }


}

module.exports = Tareas;